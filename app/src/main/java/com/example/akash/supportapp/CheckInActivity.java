package com.example.akash.supportapp;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class CheckInActivity extends AppCompatActivity {


    LinearLayout layoutCheckIn,layoutLogout,layoutSyncdata;
    Toolbar toolbar;
    DatabaseDb db ;
    DeveloperModel developerModel;
    String token;
    Context context;
    ProgressDialog progressDialog;
    int MY_PERMISSIONS_REQUEST_READ_CONTACTS=1;
    Bundle bundle;
    int flag=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);
        layoutLogout = (LinearLayout) findViewById(R.id.layoutLogout);
        layoutCheckIn = (LinearLayout) findViewById(R.id.layoutCheckIn);
        layoutSyncdata = (LinearLayout) findViewById(R.id.layoutSyncdata);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = new DatabaseDb(this);
        context = this;
        developerModel = new DeveloperModel();
        developerModel = db.getTokenFromDeveloper(1);
        token = developerModel.getToken();
        bundle = getIntent().getExtras();
        if (bundle!=null){
            flag = bundle.getInt("flag",0);
        }

        layoutCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isGPSEnable()){
                    Intent i = new Intent(CheckInActivity.this,FormActivity.class);
                    startActivity(i);
                }
            }
        });

        layoutSyncdata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CentreList().execute();
            }
        });
        layoutLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(CheckInActivity.this)
                        .setTitle("Log Out")
                        .setMessage("Do you really want to Log Out?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                new Logout().execute();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });
        getPermission();
        if (flag==1){
            new ValidateToken().execute();
        }
    }

    public boolean isGPSEnable(){
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
            return false;
        }else {
            return true;
        }
    }

    public void logOutFromApp(DatabaseDb db) {
        try {
            db.deleteAllDataFromDB();
            db.close();
            Intent intent2 = new Intent(CheckInActivity.this, LoginActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP );
            startActivity(intent2);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private class CentreList extends AsyncTask<Void, Void, Void> {

        String str;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog("Syncing data.Please wait");
        }
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                HashMap<String, String> h1 = new HashMap<>();
                h1.put("token",token);
                h1.put("devKey","1");
                str = new OkHttp3Utility().HTTP_URL_CONNECTION_POST(Utility.getLabDetailsSupport,h1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try{
                progressDialog.dismiss();
                if (str!=null && !str.equals("")){
                    JSONObject obj = new JSONObject(str);
                    int code = obj.getInt("code");
                    if (code==200){
                        JSONArray labList = obj.getJSONArray("labs");
                        if (labList.length()>0){
                            db.deleteCentreNames();
                            for (int i=0;i<labList.length();i++){
                                JSONObject childObj = labList.getJSONObject(i);
                                CentreModel centreModel = new CentreModel();
                                centreModel.setLabId(childObj.getInt("labId"));
                                centreModel.setLabName(childObj.getString("labName"));
                                centreModel.setLabArea(childObj.getString("labArea"));
                                centreModel.setLabAddress(childObj.getString("labAddress"));
                                db.insertLabDetails(centreModel);
                            }
                        }
                    }
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
     private class Logout extends AsyncTask<Void, Void, Void> {

            String str;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    HashMap<String, String> h1 = new HashMap<>();
                    h1.put("token",token);
                    str = new OkHttp3Utility().HTTP_URL_CONNECTION_POST(Utility.supportLogout,h1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                try{
                    if (str!=null && !str.equals("")){
                        JSONObject obj = new JSONObject(str);
                        int code = obj.getInt("code");
                        if (code==200){
                            logOutFromApp(db);
                        }else {
                            Toast.makeText(context, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }

    public void showDialog(String message){

        progressDialog = new ProgressDialog(CheckInActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.show();
    }



    public void getPermission(){

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(CheckInActivity.this,Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(CheckInActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {
                 ActivityCompat.requestPermissions(CheckInActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode)
        {
            case 1:
            {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context, "YES", Toast.LENGTH_SHORT).show();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(context, "No", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    class ValidateToken extends AsyncTask<Void,Void,Void>{

        String str;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog("Validating user. Please wait");
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                HashMap<String, String> h1 = new HashMap<>();
                h1.put("token",token);
                str = new OkHttp3Utility().HTTP_URL_CONNECTION_POST(Utility.validateToken,h1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try{
                progressDialog.dismiss();
                if(str!=null && !str.equals("")){
                    JSONObject mainObj = new JSONObject(str);
                    int code = mainObj.getInt("code");
                    if (code!=200){
                        logOutFromApp(db);
                    }
                }else{
                    logOutFromApp(db);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
