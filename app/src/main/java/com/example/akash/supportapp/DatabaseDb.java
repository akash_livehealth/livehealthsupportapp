package com.example.akash.supportapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by akash on 20/08/16.
 */
public class DatabaseDb extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "supportdb";

//    LabName, LabId, LabArea, LabCity
    private static final String TABLE_CENTRE_NAME = "centreName";
    private static final String TABLE_DEVELOPER = "developer";

    private static final String LABNAME = "labName";
    private static final String LABID =   "labId";
    private static final String LABAREA = "labArea";
    private static final String LABCITY = "labCity";

    private static final String DEVELOPER_ID = "developerId";
    private static final String TOKEN = "token";
    private static final String PERSON_NAME = "personName";


    private static final String CREATE_TABLE_CENTRE_NAME = "CREATE TABLE "
            + TABLE_CENTRE_NAME + "(" + LABID + " INTEGER PRIMARY KEY, " + LABNAME + " Text, " + LABAREA + " Text, "
            + LABCITY + " Text" + ")";

    // developer table create statement
    private static final String CREATE_TABLE_DEVELOPER = "CREATE TABLE "
            + TABLE_DEVELOPER + "(" + DEVELOPER_ID + " INTEGER PRIMARY KEY, " + PERSON_NAME + " Text, "
            + TOKEN + " TEXT" + ")";


    public DatabaseDb(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CENTRE_NAME);
        db.execSQL(CREATE_TABLE_DEVELOPER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CENTRE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEVELOPER);
    }


    public void deleteAllDataFromDB() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_CENTRE_NAME);
        db.execSQL("DELETE FROM " + TABLE_DEVELOPER);
    }

    public void insertTokenInDeveloper(DeveloperModel developer) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DEVELOPER_ID, developer.getDeveloperId());
        values.put(TOKEN, developer.getToken());
        values.put(PERSON_NAME,developer.getPersonName());
        db.insert(TABLE_DEVELOPER, null, values);
        db.close();
    }


    public DeveloperModel getTokenFromDeveloper(int dev_id) {

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_DEVELOPER + " WHERE " + DEVELOPER_ID + "=" + dev_id;
        Cursor cur = db.rawQuery(selectQuery, null);
        DeveloperModel d = null;
        if (cur != null && cur.getCount() > 0) {
            cur.moveToFirst();
            d = new DeveloperModel();
            d.setDeveloperId(cur.getInt(cur.getColumnIndex(DEVELOPER_ID)));
            d.setToken(cur.getString(cur.getColumnIndex(TOKEN)));
            d.setPersonName(cur.getString(cur.getColumnIndex(PERSON_NAME)));
        }
        cur.close();
        db.close();
        return d;
    }

    public void insertLabDetails(CentreModel model) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LABID, model.getLabId());
        values.put(LABNAME, model.getLabName());
        values.put(LABAREA, model.getLabArea());
        values.put(LABCITY, model.getLabAddress());
        db.insert(TABLE_CENTRE_NAME, null, values);
        db.close();
    }


    public String[] getAllCentreNames(){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c ;
        String query = "SELECT " + LABNAME + " FROM " + TABLE_CENTRE_NAME;
        c = db.rawQuery(query,null);
        String [] labNames = new String[c.getCount()];
        if (c!=null && c.getCount()>0){
            c.moveToFirst();
            for (int i=0;i<c.getCount();i++){
                labNames[i]= c.getString(c.getColumnIndex(LABNAME));
                c.moveToNext();
            }
        }
        return labNames;
    }

    public void deleteCentreNames(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_CENTRE_NAME;
        db.execSQL(query);
    }

    public int getCentreId(String centreName){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c;
        String query = "SELECT " + LABID + " FROM " + TABLE_CENTRE_NAME + " WHERE "  + LABNAME + " = " +   "'" + centreName + "'";
        c = db.rawQuery(query,null);
        int centreId=0 ;
        if (c.moveToFirst()){
            centreId = c.getInt(c.getColumnIndex(LABID));
        }
        db.close();
        return centreId;
    }
}
