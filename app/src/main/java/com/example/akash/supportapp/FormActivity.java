package com.example.akash.supportapp;


import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.Calendar;
import java.util.HashMap;

public class FormActivity extends AppCompatActivity {


    public static final String[] activityTypeArray = new String[]{"Field Support", "Training", "Interfacing", "QC Visit", "Tasks", "Other"};
    public String[] centreNameArray;

    Spinner  spinnerActivityType;
    Context context;
    LinearLayout layoutStartTime, layoutEndTime;
    Toolbar toolbar;
    String startTime, endTime;
    TextView txtEndTime, txtStartTime;
    LinearLayout layoutSubmit;
    EditText etActivityName,etContactPerson,etTaskToDo,etTaskDone,etNotes,etAccompaniedBy ;
    String activityName,contactPerson,taskToDo,taskDone,notes,accompaniedBy,activityType,labLocation,centreName;
    String centreId;
    String token;
    JSONObject primaryData;
    String startDateTime,endDateTime;
    DatabaseDb db;
    ArrayAdapter adapterCentrenName;
    ArrayAdapter adapterActivityType;
    AutoCompleteTextView autoCompleteCentreName;
    String personName;
    DeveloperModel developerModel;
    ProgressDialog progressDialog;
    TextView txtCentrename,txtActivityname,txtContactperson,txtTaskToDo,txtTaskDone,txtNotes,txtAccompaniedBy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        context = this;

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        isGPSEnable();

        spinnerActivityType = (Spinner) findViewById(R.id.spinnerActivityType);
        autoCompleteCentreName = (AutoCompleteTextView) findViewById(R.id.autoCompleteCentreName);

        etActivityName = (EditText) findViewById(R.id.etActivityName);
        etContactPerson = (EditText) findViewById(R.id.etContactPerson);
        etTaskToDo = (EditText) findViewById(R.id.etTaskToDo);
        etTaskDone = (EditText) findViewById(R.id.etTaskDone);
        etNotes = (EditText) findViewById(R.id.etNotes);
        etAccompaniedBy = (EditText) findViewById(R.id.etAccompaniedBy);

        db = new DatabaseDb(this);
        centreNameArray = db.getAllCentreNames();
        developerModel = new DeveloperModel();
        developerModel = db.getTokenFromDeveloper(1);
        token = developerModel.getToken();
        personName = developerModel.getPersonName();

        adapterCentrenName = new ArrayAdapter(context, android.R.layout.simple_dropdown_item_1line, centreNameArray);
        autoCompleteCentreName.setAdapter(adapterCentrenName);

        adapterActivityType = new ArrayAdapter(context, android.R.layout.simple_dropdown_item_1line, activityTypeArray);
        spinnerActivityType.setAdapter(adapterActivityType);

        layoutStartTime = (LinearLayout) findViewById(R.id.layoutStartTime);
        layoutEndTime = (LinearLayout) findViewById(R.id.layoutEndTime);
        layoutSubmit = (LinearLayout) findViewById(R.id.layoutSubmit);

        txtEndTime = (TextView) findViewById(R.id.txtEndTime);
        txtStartTime = (TextView) findViewById(R.id.txtStartTime);

        txtCentrename = (TextView) findViewById(R.id.txtCentrename);
        txtActivityname = (TextView) findViewById(R.id.txtActivityname);
        txtContactperson = (TextView) findViewById(R.id.txtContactperson);
        txtTaskToDo = (TextView) findViewById(R.id.txtTaskToDo);
        txtTaskDone = (TextView) findViewById(R.id.txtTaskDone);
        txtNotes = (TextView) findViewById(R.id.txtNotes);
        txtAccompaniedBy = (TextView) findViewById(R.id.txtAccompaniedBy);

        layoutStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                mcurrentTime.set(Calendar.AM_PM, Calendar.AM);
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                final int minute = 0;
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(FormActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        txtStartTime.setText(String.format("%02d", selectedHour) + ":" + String.format("%02d", selectedMinute)+ ":00");
                        startTime = txtStartTime.getText().toString();
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        layoutEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                mcurrentTime.set(Calendar.AM_PM, Calendar.AM);
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = 0;
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(FormActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        txtEndTime.setText(String.format("%02d", selectedHour) + ":" + String.format("%02d", selectedMinute)+ ":00");
                        endTime = txtEndTime.getText().toString();
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        layoutSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
            }
        });

        spinnerActivityType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                activityType = String.valueOf(adapterActivityType.getItem(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        autoCompleteCentreName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                centreId = String.valueOf(db.getCentreId(autoCompleteCentreName.getText().toString()));
                centreName = autoCompleteCentreName.getText().toString();
            }
        });

        editTextFocusListener();
    }

    public void getLocation(){
        GPSTracker gps = new GPSTracker(FormActivity.this);
        if (gps.canGetLocation()) {
           double latitude = gps.getLatitude();
           double longitude = gps.getLongitude();
           labLocation = latitude + "," + longitude;
        }
    }

    public void getData(){

        getLocation();
        activityName = etActivityName.getText().toString();
        contactPerson = etContactPerson.getText().toString();
        taskToDo = etTaskToDo.getText().toString();
        taskDone = etTaskDone.getText().toString();
        notes = etNotes.getText().toString();
        accompaniedBy = etAccompaniedBy.getText().toString();

        final Calendar c = Calendar.getInstance();
        int yy = c.get(Calendar.YEAR);
        int mm = c.get(Calendar.MONTH)+1;
        int dd = c.get(Calendar.DAY_OF_MONTH);

        startDateTime = yy +"-"+ mm +"-" + dd +"T" + startTime + "Z" ;
        endDateTime = yy +"-"+ mm +"-" + dd +"T" + endTime + "Z" ;

        validation();
    }

    public void validation(){
        if (isEmpty(autoCompleteCentreName)|| centreName==null || centreName.equals("")){
            autoCompleteCentreName.setError("Please type centre name");
        }else{
            if (isEmpty(etActivityName)|| activityName == null || activityName.equals("")){
                etActivityName.setError("Please enter activity name");
            }else{
                if (isEmpty(etContactPerson)|| contactPerson == null || contactPerson.equals("")){
                    etContactPerson.setError("Please enter contact person name.");
                }else{
                    if (isEmpty(etTaskToDo)|| taskToDo == null || taskToDo.equals("")){
                        etTaskToDo.setError("Please enter task to be done.");
                    }else {
                        if (isEmpty(etTaskDone)|| taskDone == null || taskDone.equals("")){
                            etTaskDone.setError("Please enter completed task.");
                        }else {
                            if (isEmpty(etNotes)|| notes == null || notes.equals("")){
                                etNotes.setError("Please enter notes.");
                            }else {
                                if (isEmpty(etAccompaniedBy)|| accompaniedBy == null || accompaniedBy.equals("")){
                                    etAccompaniedBy.setError("Please enter name of accompanied person.");
                                }else {
                                    if (labLocation==null || labLocation.equals("0.0,0.0")){
                                        Toast.makeText(context,"Location not found,Please try again.", Toast.LENGTH_SHORT).show();
                                    }else {
                                        if (startTime!=null && !startTime.equals("") && endTime!=null && !endTime.equals("")){
                                            createJson();
                                        }else{
                                            Toast.makeText(context,"Please select start and end time.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }


    public void editTextFocusListener() {

        autoCompleteCentreName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    txtCentrename.setTextColor(Color.parseColor("#42A5F5"));
                } else {
                    txtCentrename.setTextColor(Color.parseColor("#818181"));
                }
            }
        });
        etActivityName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    txtActivityname.setTextColor(Color.parseColor("#42A5F5"));
                } else {
                    txtActivityname.setTextColor(Color.parseColor("#818181"));
                }
            }
        });

        etContactPerson.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    txtContactperson.setTextColor(Color.parseColor("#42A5F5"));
                } else {
                    txtContactperson.setTextColor(Color.parseColor("#818181"));
                }
            }
        });
        etTaskToDo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    txtTaskToDo.setTextColor(Color.parseColor("#42A5F5"));
                } else {
                    txtTaskToDo.setTextColor(Color.parseColor("#818181"));
                }
            }
        });

        etTaskDone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    txtTaskDone.setTextColor(Color.parseColor("#42A5F5"));
                } else {
                    txtTaskDone.setTextColor(Color.parseColor("#818181"));
                }
            }
        });

        etNotes.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    txtNotes.setTextColor(Color.parseColor("#42A5F5"));
                } else {
                    txtNotes.setTextColor(Color.parseColor("#818181"));
                }
            }
        });

        etAccompaniedBy.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    txtAccompaniedBy.setTextColor(Color.parseColor("#42A5F5"));
                } else {
                    txtAccompaniedBy.setTextColor(Color.parseColor("#818181"));
                }
            }
        });

    }


    private boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() == 0;
    }

    public void createJson() {
        primaryData = new JSONObject();
        try {
            primaryData.put("centreId", centreId);
            primaryData.put("centreName", centreName);
            primaryData.put("personName", personName);
            primaryData.put("activityType", activityType);
            primaryData.put("activityName", activityName);
            primaryData.put("startTime", startDateTime);
            primaryData.put("endTime", endDateTime);
            primaryData.put("contactPerson", contactPerson);
            primaryData.put("taskToBeDone", taskToDo);
            primaryData.put("taskDone", taskDone);
            primaryData.put("notes", notes);
            primaryData.put("accompaniedBy", accompaniedBy);
            primaryData.put("labLocation", labLocation);

            new CheckIn().execute();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private class CheckIn extends AsyncTask<Void, Void, Void> {

        String str;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog();
        }
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                HashMap<String, String> h1 = new HashMap<>();
                h1.put("token",token);
                h1.put("data", String.valueOf(primaryData));
                str = new OkHttp3Utility().HTTP_URL_CONNECTION_POST(Utility.supportCheckIn,h1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            if (str!=null&& !str.equals("")){
                try{
                    JSONObject mainObj = new JSONObject(str);
                    int code = mainObj.getInt("code");
                    if (code==200){
                        Intent i = new Intent(FormActivity.this,CheckInActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
                    }else{
                        Toast.makeText(context,"Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(context,"Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void isGPSEnable(){
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        };
    }

    public void showDialog(){

        progressDialog = new ProgressDialog(FormActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Submitting details.");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.show();
    }

}
