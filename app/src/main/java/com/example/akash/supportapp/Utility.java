package com.example.akash.supportapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by akash on 20/08/16.
 */
public class Utility {

    public static volatile String mainUrl = "https://livehealth.solutions/";
//    public static volatile String mainUrl = "http://beta.livehealth.solutions/";
//    public static volatile String mainUrl = "http://192.168.2.17:8000/";

    public static volatile String loginUrl = mainUrl + "loginSupport/";
    public static volatile String supportCheckIn = mainUrl + "supportCheckIn/";
    public static volatile String getLabDetailsSupport = mainUrl + "getLabDetailsSupport/";
    public static volatile String supportLogout = mainUrl + "supportLogout/";
    public static volatile String validateToken = mainUrl + "validateToken/";

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < 2; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }
}
