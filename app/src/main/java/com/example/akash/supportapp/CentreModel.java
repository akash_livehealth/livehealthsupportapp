package com.example.akash.supportapp;

/**
 * Created by akash on 20/08/16.
 */
public class CentreModel {

    String labName;
    String labArea;
    String labAddress;
    int labId;

    public String getLabName() {
        return labName;
    }

    public void setLabName(String labName) {
        this.labName = labName;
    }

    public String getLabArea() {
        return labArea;
    }

    public void setLabArea(String labArea) {
        this.labArea = labArea;
    }

    public String getLabAddress() {
        return labAddress;
    }

    public void setLabAddress(String labAddress) {
        this.labAddress = labAddress;
    }

    public int getLabId() {
        return labId;
    }

    public void setLabId(int labId) {
        this.labId = labId;
    }
}
