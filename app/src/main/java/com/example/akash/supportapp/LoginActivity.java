package com.example.akash.supportapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {

    Button btnLogin;
    String name,pass;
    EditText etUsername,etPassword;
    Context context;
    DatabaseDb db;
    ProgressDialog progressDialog;
    TextView txtpassword,txtLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context =this;
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);

        txtLogin = (TextView) findViewById(R.id.txtLogin);
        txtpassword = (TextView) findViewById(R.id.txtpassword);

        db = new DatabaseDb(this);
        editTextFocusListener();
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    login();
                }
                return true;
            }
        });
    }

    public void editTextFocusListener() {

        etUsername.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    txtLogin.setTextColor(Color.parseColor("#42A5F5"));
                } else {
                    txtLogin.setTextColor(Color.parseColor("#818181"));
                }
            }
        });
        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    txtpassword.setTextColor(Color.parseColor("#42A5F5"));
                } else {
                    txtpassword.setTextColor(Color.parseColor("#818181"));
                }
            }
        });
    }





    public void login(){

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etUsername.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
        if (Utility.isConnectingToInternet(context)) {
            name = etUsername.getText().toString().trim();
            pass = etPassword.getText().toString();
            try {
                if (name.equals("")) {
                    etUsername.setError("Enter username");
                    if (pass.equals("")){
                        etPassword.setError("Enter Password");
                    }
                } else if (pass.equals("")) {
                    etPassword.setError("Enter Password");
                }
                else {
                    new Login().execute();
                }
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Server Unreachable or Check Your internet Connection...", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(),"You are not connected to the Internet. Please try again.",Toast.LENGTH_SHORT).show();
        }

    }

    private class Login extends AsyncTask<Void, Void, Void> {

        String str;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog();
        }
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                HashMap<String, String> h1 = new HashMap<>();

                h1.put("username", name);
                h1.put("password",pass);
                h1.put("devKey","1");
                str = new OkHttp3Utility().HTTP_URL_CONNECTION_POST(Utility.loginUrl,h1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try{
                progressDialog.dismiss();
                if (str!=null && !str.equals("")){
                    JSONObject obj = new JSONObject(str);
                    int code = obj.getInt("code");
                    if (code==200){
                        DeveloperModel developer = new DeveloperModel();
                        developer.setToken(obj.getString("token"));
                        developer.setPersonName(obj.getString("name"));
                        developer.setDeveloperId(1);
                        db.insertTokenInDeveloper(developer);
                        new CentreList(obj.getString("token")).execute();
                        Intent i = new Intent(LoginActivity.this,CheckInActivity.class);
                        startActivity(i);
                        finish();
                    }else if(code==400) {
                        Toast.makeText(context,"Username or Password is incorrect.",Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(context,"Something went wrong. Please try again.",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(context,"Something went wrong. Please try again.",Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private class CentreList extends AsyncTask<Void, Void, Void> {

        String str;
        String token;

        CentreList(String token){
            this.token= token;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                HashMap<String, String> h1 = new HashMap<>();
                h1.put("token",token);
                h1.put("devKey","1");
                str = new OkHttp3Utility().HTTP_URL_CONNECTION_POST(Utility.getLabDetailsSupport,h1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try{
                if (str!=null && !str.equals("")){
                    JSONObject obj = new JSONObject(str);
                    int code = obj.getInt("code");
                    if (code==200){
                        JSONArray labList = obj.getJSONArray("labs");
                        if (labList.length()>0){
                            for (int i=0;i<labList.length();i++){
                                JSONObject childObj = labList.getJSONObject(i);
                                CentreModel centreModel = new CentreModel();
                                centreModel.setLabId(childObj.getInt("labId"));
                                centreModel.setLabName(childObj.getString("labName"));
                                centreModel.setLabArea(childObj.getString("labArea"));
                                centreModel.setLabAddress(childObj.getString("labAddress"));
                                db.insertLabDetails(centreModel);
                            }
                        }
                    }
                }
            }catch(Exception e){
               e.printStackTrace();
            }
        }
    }

    public void showDialog(){

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Verifying Credentials");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.show();
    }

}
